# The data object will hold the ID of the image. We can use this data object to retrieve an image ID through its name
data "openstack_images_image_v2" "instance_image" {
  name        = var.image_name
  most_recent = true
}

# This resource adds the image name to the Terraform state of the module.
# We need to track this in-state because we want to recreate the VM on name change, but not on ID change.
resource "terraform_data" "image_name" {
  input = var.image_name
}
