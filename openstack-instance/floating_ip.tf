# Assign a floating IP to the instance if the 'expose_public_ip' variable is set to true
resource "openstack_networking_floatingip_v2" "floating_ip" {
  pool        = var.network.external_network
  description = "Public IP for the VM"

  # Only generate a floating IP if the instance should have a public IP and if the user did not already provide an IP
  count = var.network.expose_public_ip && var.network.floating_ip == null ? 1 : 0
}

# Attaches the floating IP to the instance
resource "openstack_networking_floatingip_associate_v2" "floating_ip_association" {
  # If a floating IP is provided to the module, use it. Otherwise, use the floating IP generated within the module.
  floating_ip = var.network.floating_ip == null ? openstack_networking_floatingip_v2.floating_ip[0].address : var.network.floating_ip.address
  port_id     = openstack_compute_instance_v2.instance.network[0].port

  count = var.network.expose_public_ip ? 1 : 0
}
