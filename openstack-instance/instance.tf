resource "openstack_compute_instance_v2" "instance" {
  name = var.instance_name
  # Only certain core and memory combinations are available on OpenStack
  flavor_name = "digi.v1.vm.${var.resources.cores}-${var.resources.memory_GB}-0"

  network {
    port = openstack_networking_port_v2.instance_port.id
  }

  // Boot volume based on an OS image
  block_device {
    uuid                  = data.openstack_images_image_v2.instance_image.id
    source_type           = "image"
    volume_size           = var.resources.boot_disk_GB
    boot_index            = 0
    destination_type      = "volume"
    delete_on_termination = true
    volume_type           = var.boot_disk_type
  }

  // Extra volumes, which are initially empty
  dynamic "block_device" {
    for_each = openstack_blockstorage_volume_v3.instance_extra_volumes
    content {
      uuid                  = block_device.value.id
      source_type           = "volume"
      boot_index            = block_device.key + 1
      destination_type      = "volume"
      guest_format          = var.extra_disks[block_device.key].filesystem
      delete_on_termination = false
    }
  }

  user_data = lookup(local.config, var.config.type, null)

  depends_on = [
    # Recreate the instance when the image name changes
    terraform_data.image_name,
  ]

  lifecycle {
    ignore_changes = [
      # Do not recreate the instance when the image ID changes.
      # We don't want to recreate the instance just because the image was updated.
      block_device.0.uuid,
    ]
  }
}
