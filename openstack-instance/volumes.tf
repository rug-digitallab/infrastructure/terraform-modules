resource "openstack_blockstorage_volume_v3" "instance_extra_volumes" {
  count       = length(var.extra_disks)
  name        = "${var.instance_name}-extra-volume-${count.index}"
  description = "This volume is an extra disk attached to the ${var.instance_name} instance"
  size        = var.extra_disks[count.index].size_GB
  volume_type = var.extra_disks[count.index].type
}
