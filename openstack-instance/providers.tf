terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 3.0.0"
    }

    # Butane config parsing
    ct = {
      source  = "poseidon/ct"
      version = ">= 0.13.0"
    }
  }
}
