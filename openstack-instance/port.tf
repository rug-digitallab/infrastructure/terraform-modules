resource "openstack_networking_port_v2" "instance_port" {
  name = "${var.instance_name}-port"

  network_id         = var.network.network_id
  admin_state_up     = "true"
  security_group_ids = var.network.security_group_ids

  # Without this, the port will be created in parallel with the subnet, which will fail because it cannot get an IP address without the subnet being present
  depends_on = [
    data.openstack_networking_subnet_v2.instance_subnet
  ]
}

data "openstack_networking_subnet_v2" "instance_subnet" {
  subnet_id = var.network.subnet_id
}
