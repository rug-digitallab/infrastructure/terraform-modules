# Cloud-init configuration parsing
data "cloudinit_config" "cloudinit_config" {
  dynamic "part" {
    for_each = var.config.snippets
    content {
      content_type = "text/cloud-config"
      content      = part.value
    }
  }

  count = var.config.type == "cloud-init" ? 1 : 0
}


# Butane -> Ignition mapping and Ignition configuration parsing
data "ct_config" "butane_config" {
  strict = false
  # Does not need to be human readable, so compact the output
  pretty_print = false

  content = file("${path.module}/config/base_butane.yaml")

  snippets = var.config.snippets

  count = var.config.type == "butane" ? 1 : 0
}

# Config lookup table
locals {
  config = {
    cloud-init = var.config.type == "cloud-init" ? try(data.cloudinit_config.cloudinit_config[0].rendered, null) : null
    butane     = var.config.type == "butane" ? try(data.ct_config.butane_config[0].rendered, null) : null
  }
}
