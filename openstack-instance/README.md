# Terraform Module for a single Virtual Machine within OpenStack
This is a Terraform Module that will create a virtual machine on OpenStack. It also supports:
- Security Groups
- Resource limits
- Multiple attached disks
- Image retrieving by name
- Support for initial configs on startup:
    - cloud-init
    - Butane
- External IP

Security groups need to be created separately.

## Inputs
- `instance_name` : The desired name of the instance. All resources created on OpenStack will contain this name.
- `image_name` : The name of the image to use for the instance. Will be looked up within the OpenStack project.
- `network` : The settings for attaching the VM to the network:
    - `network_id` : The ID of the network to attach the VM to.
    - `subnet_id` : The ID of the subnet to attach the VM to.
    - `security_group_ids` : A list of security groups to attach to the VM.
    - `expose_public_ip` : Whether to expose the VM to the public internet. A floating IP will be created and attached to the VM.
    - `external_network` : The name of the external network to retrieve a floating IP from. Should still be set even if `expose_public_ip` is false.
- `resources` : The resource limits for the VM:
    - `cores` : The number of CPU cores to allocate to the VM.
    - `memory_GB` : The amount of memory in GB to allocate to the VM.
    - `boot_disk_GB` : The size of the boot disk in GB.
- `extra_disks` : A list of additional disks to attach to the VM:
    - `size_GB` : The size of the disk in GB.
    - `filesystem` : The filesystem to format the disk with. E.g. `ext4`, `xfs`, `swap`, etc.
- `config` : The startup configuration to apply to the VM:
    - `type` : The type of configuration to apply. Either `cloud-init` or `butane`.
    - `snippets` : A list of configuration snippets to apply to the VM. The format of these snippets will depend on the `type`.

## Outputs
- `public_ip` : The public IPv4 address of this instance available from the internet. Null if `expose_public_ip` is false.
- `private_ip` : The private IPv4 address of this instance.
