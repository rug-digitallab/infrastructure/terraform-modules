output "public_ip" {
  value       = var.network.expose_public_ip && var.network.floating_ip == null ? openstack_networking_floatingip_v2.floating_ip[0].address : null
  description = "The public IPv4 address of this instance available from the internet. Requires 'expose_public_ip' to be set to true."
}

output "private_ip" {
  value       = openstack_compute_instance_v2.instance.network[0].fixed_ip_v4
  description = "The private IPv4 address of this instance."
}

output "instance_id" {
  value       = openstack_compute_instance_v2.instance.id
  description = "The id of the openstack_compute_instance_v2 deployed on OpenStack as part of this module"
}