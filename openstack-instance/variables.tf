variable "instance_name" {
  type        = string
  description = "The name of the instance"
}

variable "image_name" {
  type        = string
  description = "The name of the image to install onto the boot volume"
}

variable "network" {
  type = object({
    network_id         = string
    subnet_id          = string
    security_group_ids = list(string)
    expose_public_ip   = optional(bool, false) # False by default
    external_network   = optional(string)      # Null by default
    # One can pass a openstack_networking_floatingip_v2 resource here.
    # We cannot just ask for the address, as the address cannot be determined before the resource is created, so we cannot differentiate between null and an undetermined address.
    # Asking for the entire floating IP and only using the address ensures that we can differentiate between a yet-to-be-created floating IP (non-null object with a possibly null address) and no floating IP passed (null object).
    floating_ip = optional(object({ address = string })) # Null by default
  })
  description = "All neworking related configuration for the instance"
}

variable "resources" {
  type = object({
    cores        = number
    memory_GB    = number
    boot_disk_GB = number
  })
  description = "How many resources to allocate for the instance"
}

variable "boot_disk_type" {
  type        = string
  description = "The type of the boot disk volume within OpenStack (e.g. 'ceph-ssd' or 'ceph-hdd', depends on types available in the project)"
  default     = "ceph-hdd"
}

variable "extra_disks" {
  type = list(object({
    size_GB    = number
    filesystem = string
    type       = string
  }))
  default     = []
  description = "Extra disks to attach to the instance"
}

variable "config" {
  type = object({
    type     = string
    snippets = list(string)
  })

  validation {
    condition     = var.config.type == "cloud-init" || var.config.type == "butane"
    error_message = "config.type must be either 'cloud-init' or 'butane'"
  }

  description = "The startup configuration to apply to the instance"
}
