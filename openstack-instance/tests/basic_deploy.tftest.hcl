provider "openstack" {
  auth_url = "https://hb-openstack.hpc.rug.nl:5000"
  region   = "RegionOne"
}

run "get_external_network" {
  module {
    source = "./tests/setup/external_network"
  }
}

# A basic instance can be deployed to OpenStack
run "basic_deploy" {
  module {
    source = "./"
  }
  variables {
    instance_name = "test-instance"
    image_name    = "ubuntu-24.04-noble"
    config = {
      type     = "cloud-init"
      snippets = ["#cloud-config"]
    }
    boot_disk_type = "ceph-hdd"
    resources = {
      cores        = 1
      memory_GB    = 2
      boot_disk_GB = 10
    }
    network = {
      network_id         = run.get_external_network.network.id
      subnet_id          = run.get_external_network.network.subnets[0]
      security_group_ids = []
      expose_public_ip   = false
      external_network   = "external"
    }
  }
}
