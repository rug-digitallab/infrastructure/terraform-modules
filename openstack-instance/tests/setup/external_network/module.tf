# This module retrieves the external public network to use in the tests.
terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 3.0.0"
    }
  }
}

data "openstack_networking_network_v2" "network" {
  name = "external"
}

output "network" {
  value = data.openstack_networking_network_v2.network
}
