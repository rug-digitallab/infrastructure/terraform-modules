# This module creates a floating IP in OpenStack.
terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 3.0.0"
    }
  }
}

resource "openstack_networking_floatingip_v2" "floating_ip" {
  pool = "external"
}

output "floating_ip" {
  value = openstack_networking_floatingip_v2.floating_ip
}
