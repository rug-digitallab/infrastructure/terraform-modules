provider "openstack" {
  auth_url = "https://hb-openstack.hpc.rug.nl:5000"
  region   = "RegionOne"
}

run "deploy_test_network" {
  module {
    source = "../openstack-network"
  }
  variables {
    name            = "floating-ip-test-network"
    default_gateway = "external"
  }
}

run "create_floating_ip" {
  module {
    source = "./tests/setup/floating_ip"
  }
}

# Providing a floating IP to the module is tested here.
run "deploy_instance_with_provided_floating_ip" {
  module {
    source = "./"
  }
  variables {
    instance_name = "test-instance"
    image_name    = "ubuntu-24.04-noble"
    config = {
      type     = "cloud-init"
      snippets = ["#cloud-config"]
    }
    boot_disk_type = "ceph-hdd"
    resources = {
      cores        = 1
      memory_GB    = 2
      boot_disk_GB = 10
    }
    network = {
      network_id         = run.deploy_test_network.network_id
      subnet_id          = run.deploy_test_network.subnet_id
      security_group_ids = []
      expose_public_ip   = true
      floating_ip        = run.create_floating_ip.floating_ip
    }
  }
}
