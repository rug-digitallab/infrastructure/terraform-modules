output "network_id" {
  value       = openstack_networking_network_v2.internal.id
  description = "The generated OpenStack network ID"
}

output "subnet_id" {
  value       = openstack_networking_subnet_v2.internal.id
  description = "The generated OpenStack subnet ID"
}
