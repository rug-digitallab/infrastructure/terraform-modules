provider "openstack" {
  auth_url = "https://hb-openstack.hpc.rug.nl:5000"
  region   = "RegionOne"
}

# A basic network can be deployed to OpenStack
run "multiple_routers" {
  module {
    source = "./"
  }
  variables {
    name            = "basic-test-network"
    default_gateway = "external"
    additional_networks = [
      {
        name = "external_unfiltered"
        route_cidrs = [
          "195.168.0.0/32",
          "192.162.0.0/16",
        ]
      }
    ]
  }
}
