provider "openstack" {
  auth_url = "https://hb-openstack.hpc.rug.nl:5000"
  region   = "RegionOne"
}

# A basic network can be deployed to OpenStack
run "basic_deploy" {
  module {
    source = "./"
  }
  variables {
    name            = "basic-test-network"
    default_gateway = "external"
  }
}
