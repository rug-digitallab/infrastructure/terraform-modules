terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = ">= 3.0.0"
    }
  }
}

variable "network_id" {
  type = string
}

variable "subnet_id" {
  type = string
}

resource "openstack_networking_port_v2" "port" {
  name           = "port_1"
  network_id     = var.network_id
  admin_state_up = "true"

  fixed_ip {
    subnet_id = var.subnet_id
  }
}

output "private_ip" {
  value = openstack_networking_port_v2.port.all_fixed_ips[0]
}
