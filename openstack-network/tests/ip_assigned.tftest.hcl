provider "openstack" {
  auth_url = "https://hb-openstack.hpc.rug.nl:5000"
  region   = "RegionOne"
}

# Deploy our network module
run "deploy_network" {
  module {
    source = "./"
  }
  variables {
    name            = "basic-test-network"
    default_gateway = "external"
    cidr            = "10.10.1.0/24"
  }

  assert {
    condition     = output.network_id != null
    error_message = "Network was not created successfully"
  }

  assert {
    condition     = output.subnet_id != null
    error_message = "Subnet was not created successfully"
  }
}

# Deploy our OpenStack instance module and attach to the network
run "deploy_port" {
  module {
    source = "./tests/setup/port"
  }
  variables {
    network_id = run.deploy_network.network_id
    subnet_id  = run.deploy_network.subnet_id
  }

  assert {
    condition     = startswith(output.private_ip, "10.10.1.")
    error_message = "Instance did not receive a correct private IP address"
  }
}
