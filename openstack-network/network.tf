# We create an internal network in OpenStack. The created internal
# network is linked to an external network with a router, if desired.
# Any floating IPs or security groups need to be created separately.

# We create an internal network that is not shared
resource "openstack_networking_network_v2" "internal" {
  name     = var.name
  external = false
  shared   = false
}

# For the previously-created network, we add a subnet so we can hand out
# IP addresses to clients over DHCP.
resource "openstack_networking_subnet_v2" "internal" {
  name            = "${var.name}-default"
  network_id      = openstack_networking_network_v2.internal.id
  cidr            = var.cidr
  ip_version      = 4
  dns_nameservers = var.nameservers
}
