variable "name" {
  type        = string
  description = "The desired name of the network. This name must be unique in the OpenStack project."
}

variable "cidr" {
  type        = string
  description = "The IPv4 CIDR of the subnet to use within the network."
  default     = "10.10.0.0/24"

  validation {
    condition     = can(cidrhost(var.cidr, 0)) && can(cidrnetmask(var.cidr))
    error_message = "The entered IPv4 subnet CIDR is not valid"
  }
}

variable "nameservers" {
  type        = list(string)
  description = "List of nameservers to use for all clients within the network."
  default     = ["1.1.1.1", "9.9.9.9"]
}

variable "default_gateway" {
  type        = string
  description = "The name of the external network that the main gateway of the network is connected to."
}

variable "additional_networks" {
  type = set(object({
    name        = string
    route_cidrs = set(string)
  }))
  description = "(Optional) List of additional networks to attach to this network. Destinations to these networks must be explicitly configured."
  default     = []
}
