module "default_gateway" {
  source = "./modules/router"

  default_gateway  = true
  destination_name = var.default_gateway
  source_name      = openstack_networking_network_v2.internal.name
  source_id        = openstack_networking_network_v2.internal.id
  source_subnet_id = openstack_networking_subnet_v2.internal.id
}

module "additional_routers" {
  source = "./modules/router"

  for_each = { for network in var.additional_networks : network.name => network }

  default_gateway  = false
  destination_name = each.key
  source_name      = openstack_networking_network_v2.internal.name
  source_id        = openstack_networking_network_v2.internal.id
  source_subnet_id = openstack_networking_subnet_v2.internal.id
  route_cidrs      = each.value.route_cidrs

  depends_on = [
    openstack_networking_network_v2.internal,
    openstack_networking_subnet_v2.internal,
  ]
}
