resource "openstack_networking_router_v2" "router" {
  name                = "${var.source_name}-bridge-${var.destination_name}"
  external_network_id = data.openstack_networking_network_v2.routing_destination.id
}

resource "openstack_networking_port_v2" "router_port" {
  # This is used to connect non-default gateway routers, so do not create a port if this is the default gateway
  count = var.default_gateway ? 0 : 1

  name           = "${var.source_name}-bridge-${var.destination_name}-port"
  network_id     = var.source_id
  admin_state_up = true

  fixed_ip {
    subnet_id = var.source_subnet_id
  }
}

resource "openstack_networking_router_interface_v2" "non_default_gateway_interface" {
  count = var.default_gateway ? 0 : 1

  router_id = openstack_networking_router_v2.router.id
  port_id   = openstack_networking_port_v2.router_port[0].id
}

resource "openstack_networking_router_interface_v2" "default_gateway_interface" {
  count = var.default_gateway ? 1 : 0

  router_id = openstack_networking_router_v2.router.id
  subnet_id = var.source_subnet_id
}
