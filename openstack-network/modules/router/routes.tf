resource "openstack_networking_subnet_route_v2" "subnet_route_1" {
  for_each = var.route_cidrs

  subnet_id        = var.source_subnet_id
  destination_cidr = each.value
  next_hop         = openstack_networking_port_v2.router_port[0].all_fixed_ips[0]
}
