variable "destination_name" {
  type        = string
  description = "The name of the network to route to."
}

variable "source_name" {
  type        = string
  description = "The name of the network to route from."
}

variable "source_id" {
  type        = string
  description = "The id of the network to route from."
}

variable "source_subnet_id" {
  type        = string
  description = "The name of the subnet to route from."
}

variable "default_gateway" {
  type        = bool
  description = "Whether this router should be the default gateway of the subnet."
}

variable "route_cidrs" {
  type        = set(string)
  description = "List of CIDRs that should hop through the router. Can only be used if default_gateway is false."
  default     = []
}
