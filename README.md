# Terraform Modules
This repository hosts a collection of Terraform modules used within Digital Lab. Each module is designed to be as generic as possible, allowing for easy reuse across multiple projects, such as Virtual Labs or our infrastructure.

## Modules
- [openstack-instance](openstack-instance/README.md): A Terraform Module for a single Virtual Machine within OpenStack.
- [openstack-network](openstack-network/README.md): A Terraform Module for an internal network in OpenStack.

## Usage
A module can be imported in a Terraform/OpenTofu project as follows:
```hcl
module "name" {
  source                = "gitlab.com/rug-digitallab/<module>/<provider>"
  version               = "<version>"
  <module-specific-variables>
}
```

An example for the network module is:
```hcl
module "cluster_network" {
  source                = "gitlab.com/rug-digitallab/network/openstack"
  version               = "0.0.3"
  name                  = "cluster-network"
  cidr                  = "10.10.1.0/24"
  external_network_name = "external
}
```

## Testing
All modules are tested using `OpenTofu test`. These test cases will ephemerally deploy real infrastructure to an OpenStack integration testing project, and then verify that the infrastructure is correctly deployed and functioning as expected. After the assertions are verified, the infrastructure is destroyed.

`OpenTofu test` and `Terraform test` are practically the same, but Terraform has better documentation on this. You can refer to https://developer.hashicorp.com/terraform/language/tests for more information.
